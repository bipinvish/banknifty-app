from pandas_datareader import data
from alpha_vantage.timeseries import TimeSeries
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import mplfinance as mpf

import pandas as pandas
import time
import datetime as dt

def getdata1():
    ticker = 'ˆNSEBANK'
    data1 = data.DataReader(ticker,'yahoo',dt.datetime(2014,1,1),dt.datetime(2016,11,11))
    print(data1)
    data1.to_csv(r'FileNFTY.csv')

def getdata2():
    api_key = 'G5U3BOLKZPAC27BZ'

    ts = Timeseries(key=api_key, output_format='pandas')
    data, meta_data = ts.get_intraday(symbol='MSFT', interval='1min', outputsize='full')
    print(data)
    data.to_csv(r'SampleCSV.csv')


def getdata3():
    api_key = 'G5U3BOLKZPAC27BZ'

    f = data.DataReader("AAPL", "av-intraday", start=dt.datetime(2020,6,1), end=dt.datetime(2020,6,1),api_key=api_key);
    print(f.loc["2020-06-01"])


def getdata4():
    api_key = 'G5U3BOLKZPAC27BZ'
    print(data.get_quote_av(["AAPL", "TSLA"],api_key=api_key))


def getdata5():
    ts = TimeSeries(key='G5U3BOLKZPAC27BZ', output_format='pandas')
    data, meta_data = ts.get_intraday(symbol='^NSEBANK', interval='5min', outputsize='full')
    print(meta_data)
    print(data)
    print(type(data))
    data.index.name='date'

    mpf.plot(data,type='candle')
    # data['4. close'].plot()
    # candlestick_ohlc(data, ohlc, width=0.4, colorup='#77d879', colordown='#db3f3f')
    # plt.title('Intraday TimeSeries NSEBank')
    # plt.show()

if __name__ == "__main__":
    getdata5()
